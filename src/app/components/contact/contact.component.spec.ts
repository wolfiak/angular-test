import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactComponent } from './contact.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { BrowserModule, By } from '@angular/platform-browser';
import { HomeComponent } from '../home/home.component';
import { AppComponent } from '../../app.component';
import { RouterModule } from '@angular/router';

describe('ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;

  let formDE: DebugElement;
  let formEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactComponent, HomeComponent, AppComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    formDE = fixture.debugElement.query(By.css('form'));
    formEl = formDE.nativeElement;
  });

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));
  it('shold have text', () => {
    expect(component.text).toEqual('contact page');
  });
  it('should be submited to true', () => {
    component.onSubmit();
    expect(component.submitted).toBeTruthy();
  });
  it('should call the onSubmit method', () => {
    fixture.detectChanges();
    spyOn(component, 'onSubmit');
    formEl = fixture.debugElement.query(By.css('button')).nativeElement;
    formEl.click();
    expect(component.onSubmit).toHaveBeenCalledTimes(0);
  });
  it('form should be invalid', () => {
    component.contactForm.controls['email'].setValue('');
    component.contactForm.controls['name'].setValue('');
    component.contactForm.controls['text'].setValue('');
    expect(component.contactForm.valid).toBeFalsy();
  });
  it('from should be valid', () => {
    component.contactForm.controls['email'].setValue('email@example.com');
    component.contactForm.controls['name'].setValue('darek');
    component.contactForm.controls['text'].setValue('Dupa kamieni kupa');
    expect(component.contactForm.valid).toBeTruthy();
  });
});
